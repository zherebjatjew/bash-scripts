#!/bin/bash
#
# Number of lines in code of your project.
#
# zherebjatjew@gmail.com

# Scans for all source code files
find . -type f -not \( -path '*/.git/*' \) \
  | xargs file \
  | grep ':.*ASCII text' \
  | sed 's/:.*$//' \
  | xargs sed /^$/d \
  | wc -l

