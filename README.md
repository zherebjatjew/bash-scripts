# README #

Set of bash utils.

## extlist.sh ##

List all file extensions in current directory including hidden files.

## filetotal.sh ##

Total size of files with given extensions.

## linesofcode.sh ##

Number of lines in code of your project.