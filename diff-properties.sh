#!/bin/bash

######################################################################
# Compare propery values in two files
#
# zherebjatjew@gmail.com
######################################################################

ONE=$(mktemp)
TWO=$(mktemp)

function cleanup {
  rm -f $ONE 2> /dev/null
  rm -f $TWO 2> /dev/null
}

trap cleanup EXIT

# leave only property definitions
sed -ne '/^\s*[a-zA-Z._0-9]\+\s*=\s*\S*/{s/^\s*//;s/\s*=\s*/=/;p}' $1 | sort > $ONE
sed -ne '/^\s*[a-zA-Z._0-9]\+\s*=\s*\S*/{s/^\s*//;s/\s*=\s*/=/;p}' $2 | sort > $TWO

diff $ONE $TWO
