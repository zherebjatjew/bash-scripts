#!/bin/bash
# 
# Total size of files with given extensions
# $1 $2 .. - extensions without leading dot
#
# zherebjatjew@gmail.com

TOTAL=0
while (( "$#" )); do
	TOTAL=$(echo "${TOTAL}+"$(find . -name "*.$1" | sed -e "s/^\(.*\)$/\'\1\'/" | xargs du -sb | cut -f 1 -d . | paste -s -d+) | bc)
	shift
done

echo ${TOTAL}

