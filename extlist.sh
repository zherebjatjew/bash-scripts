#!/bin/bash
#
# list all extensions of files in current directory
#
# zherebjatjew@gmail.com

find . -type f | sed -ne '/\.\w*$/{s/^.*\.\(\w*\)$/\1/;p}' | sort | uniq

